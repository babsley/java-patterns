package Delegate;

public class DelegateApp {
    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.setDeveloper(new FrontEnd());
        manager.makeSite();
        manager.setDeveloper(new BackEnd());
        manager.makeSite();
    }
}

interface Developer {
    public void makeSite();
}

class FrontEnd implements Developer {
    public void makeSite() {
        System.out.println("HTML,CSS,JS");
    }
}


class BackEnd implements Developer {
    public void makeSite() {
        System.out.println("PHP,MYSQL");
    }
}


class Manager {
    Developer developer;

    void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public void makeSite() {
        developer.makeSite();
    }
}