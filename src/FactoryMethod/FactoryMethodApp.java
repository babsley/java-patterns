package FactoryMethod;

public class FactoryMethodApp {
    public static void main(String[] args) {
        Company company = FactoryMethodApp.GetCompany("expensive");
        Site site = company.makeSite();
        site.setContent("hello world!");
        site.bringCustomers();
    }

    public static Company GetCompany(String company) {
        if (company.equals("expensive")) {
            return new ExpensiveCompany();
        } else if(company.equals("cheap")){
            return new CheapCompany();
        }

        throw new RuntimeException("Company not found");
    }
}

interface Company {
    Site makeSite();
}

class ExpensiveCompany implements Company {
    public Site makeSite() {
        return new LuxuriousSite();
    }
}

class CheapCompany implements Company {
    public Site makeSite() {
        return new CheapSite();
    }
}

interface Site {
    void setContent(String content);
    void bringCustomers();
}

class CheapSite implements Site {
    public void setContent(String content) {
        System.out.println(content);
    }

    public void bringCustomers() {
        System.out.println("10 customers");
    }
}

class LuxuriousSite implements Site {
    public void setContent(String content) {
        System.out.println(content);
    }

    public void bringCustomers() {
        System.out.println("30 customers");
    }
}