package Facade;

public class FacadeApp {
    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.makeSite();
    }
}

class Designer {
    private boolean design = false;

    public void makeDesign() {
        design = true;
        System.out.println("make design");
    }

    public boolean hasDesign() {
        return  design;
    }
}


class FrontEnd {
    private boolean frontEnd = false;

    public void makeFrontEnd(Designer designer) {
        if (designer.hasDesign()) {
            System.out.println("make frontEnd");
            frontEnd = true;
        } else {
            System.out.println("need design");
        }
    }

    public boolean hasFrontEnd() {
        return frontEnd;
    }
}

class BackEnd {
    private boolean backEnd = false;

    public void makeBackEnd(FrontEnd frontEnd) {
        if (frontEnd.hasFrontEnd()) {
            System.out.println("make backend");
            backEnd = true;
        } else {
            System.out.println("need design");
        }
    }

    public boolean hasBackEnd() {
        return backEnd;
    }
}

class Manager {
    Designer designer = new Designer();
    FrontEnd frontEnd = new FrontEnd();
    BackEnd backEnd = new BackEnd();

    public void makeSite() {
        designer.makeDesign();
        frontEnd.makeFrontEnd(designer);
        backEnd.makeBackEnd(frontEnd);
    }
}
